FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN apt-get update && apt-get -y install cron
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
COPY ./cron /etc/cron.d/task
RUN crontab /etc/cron.d/task
CMD cron && gunicorn --workers=3 --bind 0.0.0.0:8000 weather.wsgi
