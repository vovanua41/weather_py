#!/usr/bin/env bash

git rev-parse HEAD > rev.txt
git reset --hard
git pull -f

docker stop weather_py
docker rm weather_py
docker build -t weather/py:1 .
docker run -p 8000:8000 -d --name weather_py --restart=always weather/py:1
