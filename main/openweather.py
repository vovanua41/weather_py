from django.conf import settings
import requests, time, datetime
from .models import City

class Openweather():
    __key = settings.KEY_OPEN_WEATHE[0]
    __template_req_by_id="https://api.openweathermap.org/data/2.5/weather?id={}&appid={}&units=metric"

    def __init__(self,id_city):
        self.__id_city=id_city


    def get_weather(self,is_save=False):
        url_str = self.__template_req_by_id.format(self.__id_city, self.__key)
        response=requests.get(url_str)
        if response.status_code != 200:
            print(response)
            exit(1)
        json_arr=response.json()
        if is_save:
            self.save_weather(json_arr,response.content)
        return json_arr

    def save_weather(self,json_arr, content):
        city_log=City()
        city_log.name=json_arr['name']
        city_log.tempC=json_arr['main']['temp']
        city_log.weather_description=json_arr['weather'][0]['description']
        city_log.pressure=json_arr['main']['pressure']
        city_log.humidity=json_arr['main']['humidity']
        city_log.visibility=json_arr['visibility']
        city_log.wind_speed=json_arr['wind']['speed']
        city_log.wind_deg=json_arr['wind']['deg']
        city_log.weather_dt=datetime.datetime.fromtimestamp(json_arr['dt']).isoformat()
        city_log.weather_json=content
        city_log.save()


