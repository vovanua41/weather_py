from django_cron import CronJobBase, Schedule
from .models import City
from .openweather import Openweather

class WriteWeather(CronJobBase):
    RUN_EVERY_MINS = 15
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code="main.cron.WriteWeather"

    def do(self):
        weather = Openweather(706483)
        print(weather.get_weather(True))
        # weather.get_weather(True);


