from django.db import models
import datetime
# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=255)
    tempC = models.IntegerField()
    datatime_cr=models.DateTimeField(auto_now_add=True, blank=True)
    weather_description=models.CharField(max_length=255,blank=True)
    pressure=models.IntegerField(default=0,blank=True)
    humidity=models.IntegerField(default=0,blank=True)
    visibility=models.IntegerField(default=0 , blank=0)
    wind_speed=models.IntegerField(default=0,blank=True)
    wind_deg= models.IntegerField(default=0, blank=True)
    weather_dt=models.DateTimeField(blank=True)
    weather_json=models.TextField(blank=True)

    def __str__(self):
        return self.name+"_"+str(self.tempC)


